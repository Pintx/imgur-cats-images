//
//  CatsCollectionViewController.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import UIKit

class CatsCollectionViewController: UICollectionViewController {
    // MARK: - Properties
    private let reuseIdentifier = "ImgurCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private let labelsHeight: CGFloat = 60
    private let itemsPerRow: CGFloat = 2
    private var imgurImages: [ImgurImage] = []
    private var totalImages: Int = 0
    private var pageNumber: Int = 0
    
    let loadingQueue = OperationQueue()
    var loadingOperations: [IndexPath: DataLoadOperation] = [:]
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPage()
    }
    func loadPage() {
        ImgurClient.shared.searchCats(page: pageNumber) { (imageSearchResponse: ImgurSearchResponse?, errorString: String?) in
            if let errorString = errorString {
                print("Error \(errorString)")
            } else {
                //Add images to array and reload data
                DispatchQueue.main.async {
                    guard let imagesToAdd = imageSearchResponse?.data else {
                        return
                    }
                    self.imgurImages.append(contentsOf: imagesToAdd)
                    self.totalImages = self.imgurImages.count
                    self.pageNumber = self.pageNumber + 1
                    self.collectionView.prefetchDataSource = self
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

// MARK: - UICollectionViewDataSource
extension CatsCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalImages
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Preload load more data if needed
        if totalImages == indexPath.row + 30 {
            //Cancel prefeteching to prevent crash during loading of new records
            collectionView.prefetchDataSource = nil
            loadPage()
        }
        //Fill cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! ImgurCell
        let imgurImage = imgurImages[indexPath.row]
        if let imgData = imgurImage.imgData {
            cell.imageView.image = UIImage(data: imgData)
        } else {
            cell.imageView.image = nil
            cell.activityIndicator.alpha = 1
            cell.activityIndicator.startAnimating()
        }
        cell.titleLabel.text = imgurImage.title
        cell.viewsLabel.text = String(imgurImage.views!) + " views"
        return cell
        
    }
}

// MARK: - UICollectionViewDelegate
extension CatsCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? ImgurCell else { return }
        
        let updateCellClosure: (ImgurImage?) -> () = { [weak self] imgurImage in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                cell.imageView.image = UIImage(data: (imgurImage?.imgData)!)
                cell.activityIndicator.stopAnimating()
                cell.activityIndicator.alpha = 0
            }
            self.loadingOperations.removeValue(forKey: indexPath)
        }
        
        // Try to find an existing data loader
        if let dataLoader = loadingOperations[indexPath] {
            // If the data has already  been loaded
            if let imgurImage = dataLoader.imgurImage {
                DispatchQueue.main.async {
                    cell.imageView.image = UIImage(data: (imgurImage.imgData)!)
                    cell.activityIndicator.stopAnimating()
                    cell.activityIndicator.alpha = 0
                }
                loadingOperations.removeValue(forKey: indexPath)
            } else {
                // No data loaded yet, so add the completion closure to update the cell
                // once the data arrives
                dataLoader.loadingCompleteHandler = updateCellClosure
            }
        } else {
            let imgurImage = imgurImages[indexPath.row]
            let dataLoader = DataLoadOperation(imgurImage)
            // Provide the completion closure, and kick off the loading operation
            dataLoader.loadingCompleteHandler = updateCellClosure
            loadingQueue.addOperation(dataLoader)
            loadingOperations[indexPath] = dataLoader
            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Cancel not needed dataloaders
        if let dataLoader = loadingOperations[indexPath] {
            dataLoader.cancel()
            loadingOperations.removeValue(forKey: indexPath)
        }
    }
}

// MARK: - UICollectionViewDataSourcePrefetching
extension CatsCollectionViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView,
                        prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let _ = loadingOperations[indexPath] {
                continue
            }
            let imgurImage = imgurImages[indexPath.row]
            let dataLoader = DataLoadOperation(imgurImage)
            loadingQueue.addOperation(dataLoader)
            loadingOperations[indexPath] = dataLoader
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let dataLoader = loadingOperations[indexPath] {
                dataLoader.cancel()
                loadingOperations.removeValue(forKey: indexPath)
            }
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CatsCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: widthPerItem + labelsHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

