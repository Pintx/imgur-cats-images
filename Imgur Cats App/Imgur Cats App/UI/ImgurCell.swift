//
//  ImgurCell.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import Foundation
import UIKit

class ImgurCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}
