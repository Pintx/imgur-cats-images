//
//  ImgurSearchResponse.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import Foundation

struct ImgurSearchResponse: Codable {
    
    public let data: [ImgurImage]?
    public let success: Bool
    public let status: Int?

}
