//
//  DataStore.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import Foundation

class DataLoadOperation: Operation {
    var imgurImage: ImgurImage?
    var loadingCompleteHandler: ((ImgurImage) ->Void)?
    
    private var _imgurImage: ImgurImage
    
    init(_ imgurImage: ImgurImage) {
        _imgurImage = imgurImage
    }
    
    override func main() {
        if isCancelled { return }
        
        //Check if is image type or gallery. In gallery case, we get first image data
        var imageURL: String
        if _imgurImage.type == nil {
            guard let firstImage = _imgurImage.images?.first else {
                return
            }
            imageURL = firstImage.link
        } else {
            imageURL = _imgurImage.link
        }
        //Create URL and get data if necessary
        guard let url = URL(string: imageURL) else {
            return
        }
        if _imgurImage.imgData != nil {
            if let loadingCompleteHandler = self.loadingCompleteHandler {
                DispatchQueue.main.async {
                    loadingCompleteHandler(self._imgurImage)
                }
            }
        } else {
            getData(from: url) { data, response, error in
                guard let data = data, error == nil else { return }
                self._imgurImage.imgData = data
                self.imgurImage = self._imgurImage
                if let loadingCompleteHandler = self.loadingCompleteHandler {
                    DispatchQueue.main.async {
                        loadingCompleteHandler(self._imgurImage)
                    }
                }
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
