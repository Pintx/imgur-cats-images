//
//  ImgurClient.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import Foundation

class ImgurClient {
    
    // MARK: - Properties
    let baselURL = "https://api.imgur.com/3/gallery/search/top/all/"
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    static let shared = ImgurClient()
    private init() { }
    
    // MARK: - Functions
    func searchCats(page: Int, completion: @escaping (_ imageSearchResponse: ImgurSearchResponse?, _ errorString:String?) -> Void) {
        let searchTerm = "cats"
        let urlString = baselURL + "\(page)?q=\(searchTerm)"
        guard let url = URL(string: urlString) else {
            return
        }
        var request = URLRequest(url: url)
        request.addValue("Client-ID e0b88f5870efa69", forHTTPHeaderField: "Authorization")
        dataTask = defaultSession.dataTask(with: request) { data, response, error in
            if let error = error {
                let errorMessage = "DataTask error: " +
                    error.localizedDescription
                completion(nil, errorMessage)
            } else if
                let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                do{
                    let jsonDecoder = JSONDecoder()
                    let response = try jsonDecoder.decode(ImgurSearchResponse.self, from: data)
                    completion(response, nil)
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(nil, parsingError.localizedDescription)
                }
            } else {
                let errorMessage = "Error: Unauthorized"
                completion(nil, errorMessage)
            }
        }
        dataTask?.resume()
    }
}
