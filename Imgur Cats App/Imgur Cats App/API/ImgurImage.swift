//
//  ImgurImage.swift
//  Imgur Cats App
//
//  Created by Mikel Gonzalez Retamosa on 22/03/2020.
//  Copyright © 2020 Mikel Gonzalez Retamosa. All rights reserved.
//

import Foundation

class ImgurImage: Codable {
    
    public let title: String?
    public let type: String?
    public let link: String
    public let views: Int?
    public let images: [ImgurImage]?
    public var imgData: Data?
    
}

